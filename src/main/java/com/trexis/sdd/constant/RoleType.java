package com.trexis.sdd.constant;

public enum RoleType {

    MANAGER("Manager", 10),
    SENIOR_DEVELOPER("Senior Developer", 35),
    JUNIOR_DEVELOPER("Junior Developer", 25),
    QA_TESTER("QA Tester", 14);

    private String type;
    private Integer unit;

    RoleType(String type, Integer unit) {
        this.type = type;
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "RoleType{" +
                "type='" + type + '\'' +
                ", unit=" + unit +
                '}';
    }
}