package com.trexis.sdd.model;

import com.trexis.sdd.constant.RoleType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Department {
    private Integer id;
    private String name;
    private List<Employee> employeeList = new ArrayList<>();

    private List<Manager> topLevelManagerList;
    private List<Employee> topLevelManagersList;

    public Department(Integer id,  String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addTopLevelManager(Manager manager) {
        boolean topLevelManagerAdded = false;
        if (Objects.nonNull(getTopLevelManagerList()) && Objects.nonNull(manager)) {
            getTopLevelManagerList().add(manager);
            topLevelManagerAdded = true;
        }

        return topLevelManagerAdded;
    }

    public List<Employee> getTopLevelManagerList() {
        List<Employee> topLevelManagersList = getEmployeeList().stream().filter(employee -> isTopLevelManager(employee)).collect(Collectors.toList());

        return topLevelManagersList;
    }

    public void setTopLevelManagerList(List<Employee> topLevelManagersList) {
        this.topLevelManagersList = topLevelManagersList;
    }

    public boolean addEmployee(Employee employee) {
        boolean employeeAdded = false;

        if (Objects.nonNull(employee) && Objects.nonNull(getEmployeeList())) {
            RoleType roleType = employee.getRoleType();

            if (roleType == RoleType.MANAGER || roleType == RoleType.SENIOR_DEVELOPER || roleType == RoleType.JUNIOR_DEVELOPER || roleType == RoleType.QA_TESTER) {
                this.getEmployeeList().add(employee);
            }
        }
        return employeeAdded;
    }

    private List<Employee> getEmployeeList() {
        return employeeList;
    }

    private void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }


    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", topLevelManagerList=" + topLevelManagerList +
                '}';
    }

    public boolean isManagesDevelopmentGroup(Employee employee) {
        boolean managesDevelopmentGroup = false;

        if (Objects.nonNull(employee) && employee.getRoleType() == RoleType.MANAGER && employee instanceof Manager) {

            Manager manager = (Manager) employee;

            if (Objects.nonNull(manager) && Objects.nonNull(manager.getSubOrdinates()) && !manager.getSubOrdinates().isEmpty()) {

                managesDevelopmentGroup = manager.getSubOrdinates().stream().anyMatch(empl -> isNotManager(empl.getRoleType()));
            }
        }

        return  managesDevelopmentGroup;
    }

    public boolean isNotManager(RoleType roleType) {
        return roleType == RoleType.SENIOR_DEVELOPER || roleType == RoleType.JUNIOR_DEVELOPER || roleType == RoleType.QA_TESTER;
    }

    public boolean isSeniorDeveloperOrQATester(RoleType roleType) {
        return roleType == RoleType.SENIOR_DEVELOPER || roleType == RoleType.QA_TESTER;
    }

    public static boolean isTopLevelManager(Employee employee) {
        return employee != null && employee.getRoleType() == RoleType.MANAGER && employee.getReportsTo() == null ? true : false;
    }

    public int getTotalDepartmentCost() {
        int totalDepartmentCost = 0;

        if (Objects.nonNull(getEmployeeList()) && !getEmployeeList().isEmpty()) {
            List<Employee> employeeList = getEmployeeList();

            totalDepartmentCost  = employeeList.stream().filter(employee -> employee.getRoleType() == RoleType.MANAGER && isTopLevelManager(employee)).map(employee -> (Manager) employee).mapToInt(Manager::getTotalCostUnits).sum();
        }

        return totalDepartmentCost;
    }

    List<Employee> managerListWithLittleResponsibility = new ArrayList<>();

    public List<Employee> getManagerListWithLittleResponsibility() {
        managerListWithLittleResponsibility = new ArrayList<>();

        List<Employee> employeeList = getTopLevelManagerList();

        if (Objects.nonNull(employeeList) && !employeeList.isEmpty() ) {

            for (Employee employee : employeeList) {

                Employee rootEmployee = employee;

                if (Objects.nonNull(rootEmployee) && rootEmployee.getRoleType() == RoleType.MANAGER) {

                    if (!isManagesDevelopmentGroup(rootEmployee) && !hasMorethanOneManagerSubOrdinates(rootEmployee)) {
                        managerListWithLittleResponsibility.add(rootEmployee);
                    }
                    if (rootEmployee instanceof Manager) {
                        Manager rootManager = (Manager) rootEmployee;

                        if (Objects.nonNull(rootManager.getSubOrdinates()) && !rootManager.getSubOrdinates().isEmpty()) {
                            findManagerListWithLittleResponsibility(rootManager);
                        }
                    }
                }
            }
        }

        return managerListWithLittleResponsibility;
    }

    private static boolean hasMorethanOneManagerSubOrdinates(Employee employee) {
        boolean morethanOneManagerSubOrdinates = false;

        if (Objects.nonNull(employee) && employee instanceof  Manager) {

            Manager manager = (Manager)  employee;

            if (Objects.nonNull(manager) && Objects.nonNull(manager.getSubOrdinates()) && !manager.getSubOrdinates().isEmpty()) {

                long managerCount = manager.getSubOrdinates().stream().filter(m -> m.getRoleType() == RoleType.MANAGER).count();

                if (managerCount > 1) {
                    morethanOneManagerSubOrdinates = true;
                }
            }

        }

        return  morethanOneManagerSubOrdinates;
    }

    private  List<Employee> findManagerListWithLittleResponsibility(Manager manager) {

        for (Employee employee : manager.getSubOrdinates()) {
            if (Objects.nonNull(employee) && employee.getRoleType() == RoleType.MANAGER) {
                Manager managerEmployee = (Manager) employee;
                if (Objects.nonNull(managerEmployee) && Objects.nonNull(managerEmployee.getSubOrdinates()) && !managerEmployee.getSubOrdinates().isEmpty() ) {
                    if (!isManagesDevelopmentGroup(managerEmployee) && !hasMorethanOneManagerSubOrdinates(managerEmployee)) {
                        managerListWithLittleResponsibility.add(managerEmployee);
                    }
                    findManagerListWithLittleResponsibility(managerEmployee);
                }
            } else if (!Objects.isNull(employee) && employee.getRoleType() != RoleType.MANAGER) {

            }
        }

        return managerListWithLittleResponsibility;
    }

    List<Employee> underStaffedList = new ArrayList<>();

    public List<Employee> getManagerListWhoAreUnderstaffed() {
        underStaffedList = new ArrayList<>();

        List<Employee> employeeList = getTopLevelManagerList();

        if (Objects.nonNull(employeeList) && !employeeList.isEmpty() ) {

            for (Employee employee : employeeList) {

                if (Objects.nonNull(employee) && employee.getRoleType() == RoleType.MANAGER) {

                    if (employee instanceof Manager) {
                        Manager manager = (Manager) employee;

                        if (!hasMorethanOneManagerSubOrdinates(manager) && isUnderStaffed(manager)) {
                            underStaffedList.add(employee);
                        }

                        if (Objects.nonNull(manager.getSubOrdinates()) && !manager.getSubOrdinates().isEmpty()) {
                            findManagerListWhoAreUnderstaffed(manager);
                        }
                    }
                }
            }
        }

        return underStaffedList;
    }

    public boolean isUnderStaffed(Manager manager) {

        boolean underStaffed = false;

        if (Objects.nonNull(manager) && Objects.nonNull(manager.getSubOrdinates()) && !manager.getSubOrdinates().isEmpty())  {

            long count = manager.getSubOrdinates().stream().filter(employee -> isSeniorDeveloperOrQATester(employee.getRoleType())).count();

            if (count < 2) {
                underStaffed = true;
            }
        }

        return  underStaffed;
    }

    private  List<Employee> findManagerListWhoAreUnderstaffed(Manager manager) {

        for (Employee employee : manager.getSubOrdinates()) {
            if (Objects.nonNull(employee) && employee.getRoleType() == RoleType.MANAGER) {
                Manager managerEmployee = (Manager) employee;
                if (Objects.nonNull(managerEmployee) && Objects.nonNull(managerEmployee.getSubOrdinates()) && !managerEmployee.getSubOrdinates().isEmpty() ) {
                    if (!hasMorethanOneManagerSubOrdinates(manager) && isUnderStaffed(manager)) {
                        underStaffedList.add(managerEmployee);
                    }
                    findManagerListWhoAreUnderstaffed(managerEmployee);
                }
            } else if (!Objects.isNull(employee) && employee.getRoleType() != RoleType.MANAGER) {

            }
        }

        return underStaffedList;
    }

}