package com.trexis.sdd.model;

import com.trexis.sdd.constant.RoleType;

import java.util.Objects;

public class Employee {

    private String id;
    private String firstName;
    private String lastName;
    private Integer costUnit;
    private RoleType roleType;
    private Employee reportsTo;
    private Department department;

    public Employee(String firstName, String lastName, RoleType roleType, Department department) {
        this.id = createEmployeeIdBy(firstName, "", lastName);
        this.firstName = firstName;
        this.lastName = lastName;
        this.roleType = roleType;
        this.costUnit = roleType.getUnit();
        this.department = department;
        if (roleType == RoleType.MANAGER || roleType == RoleType.SENIOR_DEVELOPER || roleType == RoleType.JUNIOR_DEVELOPER || roleType == RoleType.QA_TESTER) {
            this.department.addEmployee(this);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getCostUnit() {
        return costUnit;
    }

    public void setCostUnit(Integer costUnit) {
        this.costUnit = costUnit;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public Employee getReportsTo() {
        return reportsTo;
    }

    public void setReportsTo(Employee reportsTo) {
        this.reportsTo = reportsTo;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName = '" + firstName + '\'' +
                ", lastName=  '" + lastName + '\'' +
                ", costUnit = " + costUnit +
                ", roleType = " + roleType +
                ", department = '" + department + "\'" +
                '}';
    }

    static Integer count = 0;

    public static String createEmployeeIdBy(String firstName, String middleName, String lastName) {
        String res1;

        if (Objects.nonNull(firstName) && firstName.length() >= 4) {
            res1 = firstName.substring(0, (2+1));
        } else {
            res1 = firstName;
        }

        String res2 = middleName.isEmpty() ? "0" : middleName.substring(0, 1);

        String res2_1;

        if (Objects.nonNull(lastName) && lastName.length() >= 4) {
            res2_1 = lastName.substring(0, (2+1));
        } else {
            res2_1 = lastName;
        }

        String res4 = res1 + res2 + res2_1;
        String res5 = count.toString().length() == 1 ? ("00" + count)
                : count.toString().length() == 2 ? ("0" + count) : count.toString();
        count = count + 1;
        String finalResult = res4 + res5;
        return finalResult;

    }
}