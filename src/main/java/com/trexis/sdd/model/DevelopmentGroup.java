package com.trexis.sdd.model;

import java.util.List;

public class DevelopmentGroup {

    private Long id;
    private String groupName;

    private List<Manager> managerList;

    @Override
    public String toString() {
        return "DevelopmentGroup{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", managerList=" + managerList +
                '}';
    }
}