package com.trexis.sdd.constant;

public enum DeveloperType {
    SENIOR_DEVELOPER("Senior Developer"),
    JUNIOR_DEVELOPER("Junior Developer");

    private final String type;

    DeveloperType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "DeveloperType{" +
                "type='" + type + '\'' +
                '}';
    }
}