package com.trexis.sdd.model;

import com.trexis.sdd.constant.RoleType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Manager extends Employee {

    private Integer unit;
    private List<Employee> subOrdinates = new ArrayList<>();

    private DevelopmentGroup developmentGroup;

    private Integer totalCostUnits = 0;

    public Manager(String firstName, String secondName, Department department) {
        super(firstName, secondName, RoleType.MANAGER, department);
    }

    public boolean addSubOrdinate(Employee employee) {
        boolean subOrdinateAdded = false;

        if (Objects.nonNull(getSubOrdinates()) && Objects.nonNull(employee)) {
            getSubOrdinates().add(employee);
            employee.setReportsTo(this);
            subOrdinateAdded = true;
        }

        return subOrdinateAdded;
    }

    public Integer getTotalCostUnits() {
        totalCostUnits = 0;

        if (Objects.nonNull(getSubOrdinates()) && !getSubOrdinates().isEmpty() ) {
            totalCostUnits += calculateTotalCostUnits(this);
        }

        totalCostUnits += getCostUnit();

        return totalCostUnits;
    }


    private Integer calculateTotalCostUnits(Manager manager) {

        for (Employee employee : manager.getSubOrdinates()) {
            if (Objects.nonNull(employee) && employee.getRoleType() == RoleType.MANAGER) {
                Manager managerEmployee = (Manager) employee;
                if (Objects.nonNull(managerEmployee) && Objects.nonNull(managerEmployee.getSubOrdinates()) && !managerEmployee.getSubOrdinates().isEmpty() ) {
                    totalCostUnits += managerEmployee.getCostUnit();
                    calculateTotalCostUnits(managerEmployee);
                }
            } else if (!Objects.isNull(employee) && employee.getRoleType() != RoleType.MANAGER) {
                totalCostUnits += employee.getCostUnit();
            }
        }

        return totalCostUnits;
    }

    public List<Employee> getSubOrdinates() {
        return subOrdinates;
    }

    public void setSubOrdinates(List<Employee> subOrdinates) {
        this.subOrdinates = subOrdinates;
    }

    @Override
    public String toString() {
        return "Manager { " + super.toString() + " , "
                + "subOrdinates = " + subOrdinates +
                '}';
    }
}