package com.trexis.sdd.test;

import com.trexis.sdd.constant.RoleType;
import com.trexis.sdd.model.*;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class DepartmentLibraryTest {

    public static void main(String[] args) {

        Department technicalDepartment = new Department(100, "Technical");

        Manager managerA = new Manager("A", "a", technicalDepartment);
        Developer developerA = new Developer("Developer First Name A", "Developer Second Name A", RoleType.JUNIOR_DEVELOPER, technicalDepartment);
        QATester qaTesterA = new QATester("QA Tester First Name A", "QA Tester Second Name A", technicalDepartment);

        managerA.addSubOrdinate(developerA);
        managerA.addSubOrdinate(qaTesterA);

        Manager managerB = new Manager("B", "b", technicalDepartment);
        Developer developerB = new Developer("Developer First Name B", "Developer Second Name B", RoleType.SENIOR_DEVELOPER, technicalDepartment);

        managerB.addSubOrdinate(developerB);
        managerA.addSubOrdinate(managerB);

        System.out.printf("Manager A's cost is %d units \r\n", managerA.getTotalCostUnits());
        System.out.printf("Manager B's cost is %d units \r\n", managerB.getTotalCostUnits());

        Manager managerC = new Manager("C", "c", technicalDepartment);

        Manager managerD = new Manager("D", "d", technicalDepartment);

        Developer seniorDeveloperD = new Developer("Senior Developer First Name D", "Senior Developer Second Name D", RoleType.SENIOR_DEVELOPER, technicalDepartment);
        Developer juniorDeveloperD = new Developer("Junior Developer First Name D", "Junior Developer Second Name D", RoleType.JUNIOR_DEVELOPER, technicalDepartment);
        QATester qaTesterD = new QATester("QA Tester First Name D", "QA Tester Second Name D", technicalDepartment);

        Manager managerE = new Manager("E", "e", technicalDepartment);
        Manager managerF = new Manager("F", "f", technicalDepartment);
        QATester qaTesterF = new QATester("QA Tester First Name F", "QA Tester Second Name F", technicalDepartment);

        managerC.addSubOrdinate(managerD);
        managerC.addSubOrdinate(managerE);

        managerD.addSubOrdinate(seniorDeveloperD);
        managerD.addSubOrdinate(juniorDeveloperD);
        managerD.addSubOrdinate(qaTesterD);

        managerE.addSubOrdinate(managerF);
        managerF.addSubOrdinate(qaTesterF);

        System.out.printf("Manager C's cost is %d units \r\n", managerC.getTotalCostUnits());
        System.out.printf("Manager D's cost is %d units \r\n", managerD.getTotalCostUnits());
        System.out.printf("Manager E's cost is %d units \r\n", managerE.getTotalCostUnits());
        System.out.printf("Manager F's cost is %d units \r\n", managerF.getTotalCostUnits());

        System.out.printf("The Department '%s' cost is %d units \r\n", technicalDepartment.getName(), technicalDepartment.getTotalDepartmentCost());

        List<Employee> managersWithTooLittleResponsibility = technicalDepartment.getManagerListWithLittleResponsibility();

        if (Objects.nonNull(managersWithTooLittleResponsibility) && !managersWithTooLittleResponsibility.isEmpty()) {
            managersWithTooLittleResponsibility.forEach(employee -> System.out.println("Manager " + employee.getFirstName() + " has too little responsibility"));
        } else {
            System.out.println("There are no managers with little responsibility in the department  " + technicalDepartment.getName());
        }

        List<Employee> underStaffed = technicalDepartment.getManagerListWhoAreUnderstaffed();

        if (Objects.nonNull(underStaffed) && !underStaffed.isEmpty()) {
            System.out.print("Managers ");
            StringJoiner stringJoiner = new StringJoiner(", " );
            underStaffed.forEach(employee ->  stringJoiner.add(employee.getFirstName()));
            System.out.println(stringJoiner.toString() + " have understaffed Development groups ");
        } else {
            System.out.println("There are no managers who are understaffed in the department  " + technicalDepartment.getName());
        }
    }
}
