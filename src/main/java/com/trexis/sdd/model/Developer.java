package com.trexis.sdd.model;

import com.trexis.sdd.constant.RoleType;

public final class Developer extends Employee {

    public Developer(String firstName, String lastName, RoleType roleType, Department department) {
        super(firstName, lastName, roleType, department);
    }

}