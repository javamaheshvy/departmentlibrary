package com.trexis.sdd.model;

import com.trexis.sdd.constant.RoleType;

public final class QATester extends Employee {
    public QATester(String firstName, String lastName, Department department) {
        super(firstName, lastName, RoleType.QA_TESTER, department);
    }
}